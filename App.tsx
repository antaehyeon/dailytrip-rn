import * as React from 'react';

import Navigator from '~/navigator/Navigator';

interface IApp {
  rootTag: number;
}

const App = (props: IApp) => {
  console.log('[App] props', props);
  return <Navigator />;
};

export default App;
