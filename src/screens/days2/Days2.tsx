import React from 'react';
import {View, Text, StyleSheet, ToastAndroid} from 'react-native';
import Colors from '~/assets/Colors';
import {Icon} from 'react-native-elements';
import StatusButton from '~/components/statusButton/StatusButton';
import {TouchableOpacity} from 'react-native';
import {ISplashScreen} from '~/interfaces/Interfaces';

const Days2 = (props: ISplashScreen) => {
  console.log('[Days2] props', props);

  /********************************************
    VARIABLE
  *********************************************/
  const {navigation} = props;

  /********************************************
    USER FUNCTION
  *********************************************/

  /********************************************
    STYLE SHEETS
  ********************************************/
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.white,
    },
  });

  /********************************************
    MAIN LAYOUT (RENDER)
  ********************************************/
  return (
    <View style={styles.container}>
      {/*<TripItem {...props} />*/}
      {/*<TripItem />*/}
      {/*<TripItem />*/}
      {/*<TripItem />*/}
      {/*<TripItem />*/}

      <NewTripItem {...props} />
      <NewTripItem {...props} />
      <NewTripItem {...props} />
      <NewTripItem {...props} />

    </View>
  );
};

/********************************************
    SUB LAYOUT
*********************************************/

const NewTripItem = props => {

  const {navigation} = props;

  return (
      <TouchableOpacity style={{zIndex:1}} onPress={() => navigation.navigate('tripDetail')} >
        <View style={{marginHorizontal: 30, marginBottom:40}} >
          <View style={{width:"100%", flexDirection:'row', alignItems:'center', justifyContent:'space-between'}} >
            <View style={{flexDirection:'row'}} >
              <Icon type={"material-community"} name={"airport"} />
              <Text style={{fontFamily: 'BlackHanSans-Regular', fontSize: 18, marginTop:7}}>제주공항</Text>
            </View>
            <Icon type={"font-awesome"} name={"arrow-right"} size={18} />
          </View>
          <View style={{flexDirection:'row', marginLeft:16, marginTop:4}} >
            <StatusButton
                content="123"
                iconName="like2"
                iconType="antdesign"
                iconColor={Colors.blue1}
                textColor={Colors.blue1}
                borderColor={Colors.blue1}
            />
            <StatusButton
                content="12"
                iconName="dislike2"
                iconType="antdesign"
                iconColor="red"
                textColor="red"
                borderColor="red"
            />
            <StatusButton
                content="1,000,000"
                iconName="money"
                iconType="font-awesome"
                iconColor={Colors.yellow1}
                textColor={Colors.black}
                borderColor={Colors.yellow1}
            />
          </View>
          <View style={{marginTop:16, height:32, justifyContent:'center', zIndex:0}} >
            <View style={{
              borderStyle: 'dotted',
              borderWidth: 3,
              borderRadius: 1,
              borderColor: 'grey'
            }} />
            <View style={{ position:'absolute', zIndex:1, height:24, borderRadius:50, borderWidth: 1, borderColor:"grey", backgroundColor:'white', marginLeft:24, flexDirection:'row', paddingHorizontal: 16, alignItems:'center', justifyContent:'space-around'}} >
              <Icon type={"material-community"} name={"bus-side"} color={"grey"} />
              <Text style={{fontFamily: 'S-CoreDream-1Thin', marginLeft:16, fontSize:12}} >5분</Text>
              <Text style={{fontFamily: 'S-CoreDream-1Thin', marginLeft:16, fontSize:12}} >3km</Text>
            </View>
            <View style={{ position:'absolute', zIndex:1, right: 24, height:24, alignItems:'center', backgroundColor:'white', borderRadius:50, justifyContent:'center', borderWidth:1 }} >
              <TouchableOpacity style={{zIndex:10}} >
                <View style={{flex:1, paddingHorizontal:16, borderRadius:50, justifyContent:'center'}} >
                  <Text style={{fontFamily: 'S-CoreDream-1Thin', fontSize:12, color:'black' }} >일정 추가</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>
  )
};

const TripItem = (props: ISplashScreen) => {
  console.log('[Days2] TripItem props', props);
  const {navigation} = props;
  const styles = StyleSheet.create({
    container: {
      width: '100%',
      height: 64,
      flexDirection: 'row',
    },
  });

  return (
    <TouchableOpacity onPress={() => navigation.navigate('tripDetail')}>
      <View style={styles.container}>
        <TransportationView />
        <TripInfo />
      </View>
    </TouchableOpacity>
  );
};

const TransportationView = props => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    container2: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    transportationText: {
      fontSize: 16,
      marginLeft: 4,
      marginTop: 8,
      fontFamily: 'BlackHanSans-Regular',
    },
    container3: {
      marginTop: 2,
    },
    distanceText: {
      fontFamily: 'S-CoreDream-3Light',
      fontSize: 12,
    },
    timeText: {
      fontFamily: 'S-CoreDream-3Light',
      fontSize: 12,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.container2}>
        <Icon type="font-awesome" name="bus" />
        <Text style={styles.transportationText}>버스</Text>
      </View>
      <View style={styles.container3}>
        <Text style={styles.distanceText}>3km</Text>
        <Text style={styles.timeText}>5분</Text>
      </View>
    </View>
  );
};

const TripInfo = () => {
  const styles = StyleSheet.create({
    container: {
      flex: 3,
      borderWidth: 1,
      borderColor: Colors.grey1,
      borderRadius: 8,
      paddingHorizontal: 8,
    },
    container2: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    tripTitle: {
      fontSize: 22,
      fontFamily: 'BlackHanSans-Regular',
      marginTop: 4,
    },
    container3: {
      flexDirection: 'row',
    },
    container4: {
      flex: 1,
      flexDirection: 'row',
      paddingLeft: 16,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.container2}>
        <View style={styles.container3}>
          <Icon type="material-community" name="airport" />
          <Text style={styles.tripTitle}>제주공항</Text>
        </View>
        <Icon type="font-awesome" name="arrow-right" />
      </View>
      <View style={styles.container4}>
        <StatusButton
          content="123"
          iconName="like2"
          iconType="antdesign"
          iconColor={Colors.blue1}
          textColor={Colors.blue1}
          borderColor={Colors.blue1}
        />
        <StatusButton
          content="12"
          iconName="dislike2"
          iconType="antdesign"
          iconColor="red"
          textColor="red"
          borderColor="red"
        />
        <StatusButton
          content="1,000,000"
          iconName="money"
          iconType="font-awesome"
          iconColor={Colors.yellow1}
          textColor={Colors.black}
          borderColor={Colors.yellow1}
        />
      </View>
    </View>
  );
};

export default Days2;
