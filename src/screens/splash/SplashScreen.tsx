import React, {useEffect} from 'react';
import _ from 'lodash';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {Button as ButtonElement} from 'react-native-elements';
import {ISplashScreen} from '~/interfaces/Interfaces';
import {useNavigation} from '@react-navigation/native';

const SplashScreen = (props: ISplashScreen) => {
  console.log('[SplashScreen] props', props);

  /********************************************
    VARIABLE
  *********************************************/
  const WAIT_TIME = 1500;
  const navigation = useNavigation();

  /********************************************
    USER FUNCTION
  *********************************************/
  useEffect(() => {
    _.delay(() => navigation.replace('launch', undefined), WAIT_TIME);
  }, []);

  /********************************************
    MAIN LAYOUT (RENDER)
  ********************************************/
  return (
    <SafeAreaView style={styles.rootContainer}>
      <Logo />
      <CopyRight />
    </SafeAreaView>
  );
};

/********************************************
    SUB LAYOUT
*********************************************/
const Logo = () => (
  <View style={styles.firstContainer}>
    <Text style={styles.logo}>DailyTrip</Text>
  </View>
);

const CopyRight = () => (
  <View style={styles.secondContainer}>
    <Text style={styles.copyRight}>corp. dailyCommit</Text>
  </View>
);

/********************************************
    STYLE SHEETS
*********************************************/
const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    alignItems: 'center',
  },
  firstContainer: {
    flex: 1,
  },
  secondContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  logo: {
    fontSize: 56,
    fontFamily: 'BlackHanSans-Regular',
    marginTop: '30%',
  },
  copyRight: {
    fontSize: 12,
    fontFamily: 'Arita-dotum-SemiBold_OTF',
  },
});

export default SplashScreen;
