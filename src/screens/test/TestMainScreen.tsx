import React from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';

const TestMainScreen = props => {
  console.log('[MainScreen] props', props);
  const {navigation} = props;

  return (
    <SafeAreaView style={styles.container}>
      <Button
        title="Font Screen"
        onPress={() => navigation.navigate('testFont')}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default TestMainScreen;
