import React from 'react';
import {View, Text} from 'react-native';

const TestFontScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontFamily: 'BlackHanSans-Regular', fontSize: 16}}>
        Black Han Sans 일반체
      </Text>
      <Text style={{fontFamily: 'Arita-dotum-Light_OTF'}}>
        아리따 돋움 Light
      </Text>
      <Text style={{fontFamily: 'Arita-dotum-Medium_OTF'}}>
        아리따 돋움 Medium
      </Text>
      <Text style={{fontFamily: 'Arita-dotum-Thin_OTF'}}>아리따 돋움 Thin</Text>
      <Text style={{fontFamily: 'Arita-dotum-Bold_OTF'}}>아리따 돋움 Bold</Text>
      <Text style={{fontFamily: 'Arita-dotum-SemiBold_OTF'}}>
        아리따 돋움 SemiBold
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-1Thin'}}>
        에스코어 드림 1 Thin
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-2ExtraLight'}}>
        에스코어 드림 2 ExtraLight
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-3Light'}}>
        에스코어 드림 3 Light
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-4Regular'}}>
        에스코어 드림 4 Regular
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-5Medium'}}>
        에스코어 드림 5 Medium
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-6Bold'}}>
        에스코어 드림 6 Bold
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-7ExtraBold'}}>
        에스코어 드림 7 ExtraBold
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-8Heavy'}}>
        에스코어 드림 8 Heavy
      </Text>
      <Text style={{fontFamily: 'S-CoreDream-9Black'}}>
        에스코어 드림 9 BLACK
      </Text>
    </View>
  );
};

export default TestFontScreen;
