import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {SocialIcon} from 'react-native-elements';
import {ISplashScreen} from '~/interfaces/Interfaces';
import {useNavigation} from '@react-navigation/native';

const LaunchScreen = (props: ISplashScreen) => {
  console.log('[LaunchScreen] props', props);

  const navigation = useNavigation();

  const switchMainScreen = () => navigation.replace('mainDrawer', undefined);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.firstContainer}></View>
      <View style={styles.secondContainer}>
        <LoginGoogleButton onPress={switchMainScreen} />
        <LoginFacebookButton onPress={switchMainScreen} />
      </View>
    </SafeAreaView>
  );
};

const LoginGoogleButton = props => (
  <TouchableOpacity {...props}>
    <SocialIcon title="Sign In With Google" button type="google" />
  </TouchableOpacity>
);

const LoginFacebookButton = props => (
  <TouchableOpacity {...props}>
    <SocialIcon title="Sign In With Facebook" button type="facebook" />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  firstContainer: {
    flex: 1,
  },
  secondContainer: {
    flex: 1,
    paddingHorizontal: '5%',
    justifyContent: 'flex-end',
    paddingBottom: '10%',
  },
});

export default LaunchScreen;
