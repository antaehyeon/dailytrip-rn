import React from 'react';
import Colors from '~/assets/Colors';
import {View, Text, StyleSheet, ActionSheetIOS} from 'react-native';
import {Button as ButtonElement} from 'react-native-elements';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ISplashScreen} from '~/interfaces/Interfaces';

const MainScreen = (props: ISplashScreen) => {
  console.log('[MainScreen] props', props);
  /********************************************
    VARIABLE
  *********************************************/
  const {navigation} = props;

  /********************************************
    USER FUNCTION
  *********************************************/
  const switchTemplateScreen = () => navigation.navigate('templateStack');
  const switchDayScreen = () => navigation.navigate('dayStack');
  const switchDay2Screen = () => navigation.navigate('day2Stack');
  const switchGoogleMapScreen = () => navigation.navigate('googleMapStack');

  /********************************************
    SUB LAYOUT
  *********************************************/

  /********************************************
    MAIN LAYOUT (RENDER)
  ********************************************/
  return (
    <View style={styles.container}>
      <Text style={styles.text}>여행이 없습니다</Text>
      <ButtonElement
        title="Template Preview Screen"
        type="clear"
        onPress={switchTemplateScreen}
      />
      <ButtonElement
        title="Days Screen"
        type="clear"
        onPress={switchDayScreen}
      />
      <ButtonElement
        title="Custom Days Screen"
        type="clear"
        onPress={switchDay2Screen}
      />
      <ButtonElement
        title="Google Map Screen"
        type="clear"
        onPress={switchGoogleMapScreen}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    fontSize: 16,
    fontFamily: 'S-CoreDream-3Light',
  },
});

export default MainScreen;
