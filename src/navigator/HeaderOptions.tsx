import * as React from 'react';
import {StyleSheet} from 'react-native';
import {LaunchRightButton} from '~/navigator/Component';

const styles = StyleSheet.create({
  launchHeaderTitle: {
    fontSize: 20,
    fontFamily: 'BlackHanSans-Regular',
  },
});

export const mainDrawerOptions = props => {
  console.log('[HeaderOptions]  mainDrawerOptions props', props);

  const {navigation} = props;

  return {
    title: 'DailyTrip',
    headerTitleStyle: styles.launchHeaderTitle,
    headerRight: () => <LaunchRightButton navigation={navigation} />,
  };
};
