import * as React from 'react';
import {NavigationNativeContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import MainScreen from '~/screens/main/MainScreen';
import SplashScreen from '~/screens/splash/SplashScreen';
import TestMainScreen from '~/screens/test/TestMainScreen';
import TestFontScreen from '~/screens/test/TestFontScreen';
import LaunchScreen from '~/screens/launch/LaunchScreen';

import {mainDrawerOptions} from '~/navigator/HeaderOptions';
import GuideScreen1 from '~/screens/guide/GuideScreen1';
import GuideScreen2 from '~/screens/guide/GuideScreen2';
import GuideScreen3 from '~/screens/guide/GuideScreen3';
import GuideScreen4 from '~/screens/guide/GuideScreen4';
import GuideScreen5 from '~/screens/guide/GuideScreen5';
import LoadingScreen from '~/screens/animation/AnimationScreen.tsx';
import TemplatePreview from '~/screens/template-preview/TemplatePreview';
import {Button as ButtonElement, Icon} from 'react-native-elements';
import {ISplashScreen} from '~/interfaces/Interfaces';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet, Vibration, View, Dimensions, Text} from 'react-native';
import Days from '~/screens/days/Days';
import TripDetail from '~/screens/tripDetail/TripDetail';
import Days2 from '~/screens/days2/Days2';
import googleMapScreen from '~/screens/googleMap/googleMapScreen';

const {width: deviceWidth, height: deviceHeight} = Dimensions.get('window');

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const templateStackHeaderStyle = {
  shadowRadius: 0,
  shadowOffset: {
    height: 0,
  },
  backgroundColor: '#3f6ba1',
};

const templateStackHeaderTitleStyle = {
  fontSize: 20,
  fontFamily: 'BlackHanSans-Regular',
  color: 'white',
};

const Navigator = (props: any) => {
  console.log('[Navigator] props', props);

  return (
    <NavigationNativeContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="splash"
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="launch"
          component={LaunchScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="mainDrawer"
          component={MainDrawer}
          options={mainDrawerOptions}
        />
        <Stack.Screen
          name="guideStack"
          component={GuideStack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="templateStack"
          component={TemplatePreviewStack}
          options={{
            title: '템플릿 제목',
            headerStyle: templateStackHeaderStyle,
            headerTitleStyle: templateStackHeaderTitleStyle,
            headerLeft: () => <HeaderLeftBackButton />,
          }}
        />
        <Stack.Screen
          name="dayStack"
          component={DayStack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="day2Stack"
          component={Day2Stack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={'googleMapStack'}
          component={GoogleMapStack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="testMain"
          component={TestMainScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen name="testFont" component={TestFontScreen} />
      </Stack.Navigator>
    </NavigationNativeContainer>
  );
};

const HeaderLeftBackButton = () => {
  const navigation = useNavigation();

  return (
    <ButtonElement
      type="clear"
      style={{marginLeft: 8}}
      hitSlop={{top: 8, right: 8, bottom: 8, left: 8}}
      onPress={() => navigation.goBack()}
      icon={<Icon type="ionicon" name="md-arrow-round-back" color="white" />}
    />
  );
};

const MainDrawer = () => (
  <Drawer.Navigator drawerPosition="right">
    <Drawer.Screen
      name="mainScreen"
      component={MainScreen}
      options={{drawerLabel: 'drawer'}}
    />
  </Drawer.Navigator>
);

const GuideStack = () => (
  <Stack.Navigator headerMode="none">
    <Stack.Screen name="firstGuideScreen" component={GuideScreen1} />
    <Stack.Screen name="secondGuideScreen" component={GuideScreen2} />
    <Stack.Screen name="thirdGuideScreen" component={GuideScreen3} />
    <Stack.Screen name="fourthGuideScreen" component={GuideScreen4} />
    <Stack.Screen name="fifthGuideScreen" component={GuideScreen5} />
    <Stack.Screen name="loadingScreen" component={LoadingScreen} />
  </Stack.Navigator>
);

const TemplatePreviewStack = (props: ISplashScreen) => {
  const {navigation} = props;

  return (
    <Drawer.Navigator
      drawerPosition="right"
      drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="navigatorMain" component={TemplatePreview} />
    </Drawer.Navigator>
  );
};

const DayStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="day" component={Days} />
      <Stack.Screen name="tripDetail" component={TripDetail} />
    </Stack.Navigator>
  );
};

const Day2Stack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="day" component={Days2} />
      <Stack.Screen name="tripDetail" component={TripDetail} />
    </Stack.Navigator>
  );
};

const GoogleMapStack = () => {
  return (
    <Stack.Navigator options={{headerShown: false}}>
      <Stack.Screen
        options={{headerShown: false}}
        name="googleMap"
        component={googleMapScreen}
      />
    </Stack.Navigator>
  );
};

const CustomDrawerContent = props => {
  console.log('[Navigator] CustomDrawerContent props', props);

  return (
    <View style={{flex: 1}}>
      <Text
        style={{
          fontFamily: 'BlackHanSans-Regular',
          fontSize: 24,
          marginTop: 16,
          marginLeft: 16,
        }}>
        필터 Drawer
      </Text>
    </View>
  );
};

export default Navigator;
