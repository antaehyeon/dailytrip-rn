interface INavigation {
  push(): void;
  pop(): void;
  replace: (name: string, params?: object) => void;
  navigate: (name: string, params?: object) => void;
}

interface IRoute {
  key: string;
  name: string;
  params: object | undefined;
}

export interface ISplashScreen {
  navigation: INavigation;
  route: IRoute;
}
